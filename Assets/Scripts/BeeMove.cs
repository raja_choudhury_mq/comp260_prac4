﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	// public parameters with default values
	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	// private state
	private float speed;
	private float turnSpeed;

	public Transform target;
	public Transform target2;
	private Vector2 heading = Vector2.right;
	public BeeSpawner beeSpawner;
	public ParticleSystem explosionPrefab;

	void Start()
	{
		if (target == null)
		{
			target = beeSpawner.Player1.transform;
		}
		if(target2 == null)
		{
			target2 = beeSpawner.Player2.transform;
		}

		// bee initially moves in random direction
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate(angle);

		// set speed and turnSpeed randomly 
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
								 Random.value);
	}

	void Update()
	{
		//calculate the closest player
		float playerDistance = Vector2.Distance(target.position, transform.position);
		float player2Distance = Vector2.Distance(target2.position, transform.position);

		Vector2 direction;
		if(playerDistance < player2Distance)
		{
			// get the vector from the bee to the target 
			direction = target.position - transform.position;
		}
		else
		{
			direction = target2.position - transform.position;
		}

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft(heading))
		{
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else
		{
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos()
	{
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);

		// draw target vector in green
		Gizmos.color = Color.green;
		Vector2 direction2 = target2.position - transform.position;
		Gizmos.DrawRay(transform.position, direction2);
	}

	void OnDestroy()
	{
		// create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		// destroy the particle system when it is over
		Destroy(explosion.gameObject, explosion.duration);
	}
}
