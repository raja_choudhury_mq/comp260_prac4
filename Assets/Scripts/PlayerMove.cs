﻿using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public float maxSpeed = 5.0f; // in metres per second
	public float acceleration = 1.0f; // in metres/second/second
	public float brake = 5.0f; // in metres/second/second
	public float turnSpeed = 30.0f; // in degrees/second
	public float destroyRadius = 1.0f;

	private float speed = 0.0f;    // in metres/second

	public string horizontalAxis;
	public string verticalAxis;

	private BeeSpawner beeSpawner;

	void Start()
	{
		// find the bee spawner and store a reference for later
		beeSpawner = FindObjectOfType<BeeSpawner>();
	}

	void Update()
	{
		if (Input.GetButtonDown("Fire1"))
		{
			// destroy nearby bees
			beeSpawner.DestroyBees
				(transform.position, destroyRadius);
		}

		// the horizontal axis controls the turn
		float turn = Input.GetAxis(horizontalAxis);

		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis(verticalAxis);

		//Debug.Log("Forwards: " + forwards);
		if (forwards > 0)
		{
			transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime);
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		}
		else if (forwards < 0)
		{
			transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime);
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		}
		else
		{
			// braking
			if (speed > 0)
			{
				speed = speed - brake * Time.deltaTime;
			}
			else if (speed < 0)
			{
				speed = speed + brake * Time.deltaTime;
			}
		}

		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);
		//Debug.Log("Speed: " + speed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		if(forwards == 0)
		{
			speed = 0;
		}

		// move the object
		//Debug.Log("Velocity: " + velocity);
		if (forwards != 0)
		{
			transform.Translate(velocity * Time.deltaTime, Space.Self);
		}
	}
}
